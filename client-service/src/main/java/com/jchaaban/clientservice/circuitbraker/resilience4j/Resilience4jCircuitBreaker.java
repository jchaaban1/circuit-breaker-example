package com.jchaaban.clientservice.circuitbraker.resilience4j;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.event.CircuitBreakerOnStateTransitionEvent;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.event.RetryEvent;
import io.github.resilience4j.retry.event.RetryOnSuccessEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.function.Supplier;

@Component
public class Resilience4jCircuitBreaker {

    private static final Logger logger = LoggerFactory.getLogger(Resilience4jCircuitBreaker.class);

    @Value("${person.service.base.url}")
    private String personServiceBaseUrl;

    private final RestTemplate restTemplate;
    private final CircuitBreaker circuitBreaker;
    private final Retry retry;

    public Resilience4jCircuitBreaker(RestTemplate restTemplate, CircuitBreaker circuitBreaker, Retry retry) {
        this.restTemplate = restTemplate;
        this.circuitBreaker = circuitBreaker;
        this.retry = retry;
        configureRetryEventPublisher();
        configureCircuitBreakerEventPublisher();
    }

    private void configureRetryEventPublisher() {
        retry.getEventPublisher().onRetry(this::logRetryEvent);
        retry.getEventPublisher().onSuccess(this::logSuccessEvent);
    }

    private void configureCircuitBreakerEventPublisher() {
        circuitBreaker.getEventPublisher().onStateTransition(this::logStateTransitionEvent);
    }

    public ResponseEntity<?> getPersonById(Long id) {
        Supplier<Object> supplier = () -> restTemplate.getForObject(personServiceBaseUrl + id, Object.class);

        Supplier<Object> decoratedSupplier = CircuitBreaker.decorateSupplier(circuitBreaker, supplier);
        decoratedSupplier = Retry.decorateSupplier(retry, decoratedSupplier);

        try {
            return ResponseEntity.ok(decoratedSupplier.get());
        } catch (Exception e) {
            return fallbackAfterRetry();
        }
    }

    public ResponseEntity<?> fallbackAfterRetry() {
        return ResponseEntity.status(503).body("Service is currently unavailable. Please try again later. ");
    }

    private void logRetryEvent(RetryEvent event) {
        logger.info("Retrying operation: attempt number {}", event.getNumberOfRetryAttempts());
    }

    private void logSuccessEvent(RetryOnSuccessEvent event) {
        logger.info("Operation succeeded on attempt number {}", event.getNumberOfRetryAttempts());
    }

    private void logStateTransitionEvent(CircuitBreakerOnStateTransitionEvent event) {
        logger.info("Circuit breaker {} transitioned from {} to {}",
                event.getCircuitBreakerName(),
                event.getStateTransition().getFromState(),
                event.getStateTransition().getToState());
    }
}
