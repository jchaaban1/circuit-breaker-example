package com.jchaaban.clientservice.circuitbraker.ownimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.*;

@Component
public class ClientServiceCircuitBreaker {

    private static final Logger logger = LoggerFactory.getLogger(ClientServiceCircuitBreaker.class);
    private static final int MAX_NUMBER_OF_CONSECUTIVE_FAILURES = 3;
    private static final int RETRY_TIMEOUT_MS = 1000;
    private static final int RESPONSE_WAIT_TIME_SECONDS = 3;

    private final RestTemplate restTemplate;
    @Value("${person.service.base.url}")
    private String personServiceBaseUrl;
    private long consecutiveFailuresCounter = 0;

    public ClientServiceCircuitBreaker(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public ResponseEntity<?> getPersonById(Long id) {
        CompletableFuture<ResponseEntity<?>> future = CompletableFuture.supplyAsync(() -> sendRequest(id));

        return handleResponse(future, id);
    }

    private ResponseEntity<?> sendRequest(Long id) {
        try {
            return restTemplate.getForEntity(personServiceBaseUrl + id, String.class);
        } catch (HttpClientErrorException exception) {
            return ResponseEntity.status(exception.getStatusCode()).body(exception.getMessage());
        }
    }

    private ResponseEntity<?> handleResponse(CompletableFuture<ResponseEntity<?>> future, Long id) {
        try {
            ResponseEntity<?> responseEntity = future.get(RESPONSE_WAIT_TIME_SECONDS, TimeUnit.SECONDS);
            resetFailureCounter();
            return responseEntity;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            return handleServiceFailure(id);
        }
    }
    private ResponseEntity<?> handleServiceFailure(Long id) {
        incrementFailureCounter();
        logFailure();

        if (shouldOpenCircuit()) {
            resetFailureCounter();
            return new ResponseEntity<>("Service is currently unavailable. Please try again later.", HttpStatus.SERVICE_UNAVAILABLE);
        }

        waitBeforeRetry();
        return getPersonById(id);
    }

    private void resetFailureCounter() {
        consecutiveFailuresCounter = 0;
    }

    private void incrementFailureCounter() {
        ++consecutiveFailuresCounter;
    }

    private void logFailure() {
        logger.error("Person service is not responding, number of failures " + consecutiveFailuresCounter);
    }

    private boolean shouldOpenCircuit() {
        return consecutiveFailuresCounter >= MAX_NUMBER_OF_CONSECUTIVE_FAILURES;
    }

    private void waitBeforeRetry() {
        try {
            Thread.sleep(RETRY_TIMEOUT_MS);
        } catch (InterruptedException ignored) {}
    }
}
