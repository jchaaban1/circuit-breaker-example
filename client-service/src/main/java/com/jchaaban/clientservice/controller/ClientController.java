package com.jchaaban.clientservice.controller;

import com.jchaaban.clientservice.circuitbraker.ownimpl.ClientServiceCircuitBreaker;
import com.jchaaban.clientservice.circuitbraker.resilience4j.Resilience4jCircuitBreaker;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class ClientController {

    private final ClientServiceCircuitBreaker clientServiceCircuitBreaker;
    private final Resilience4jCircuitBreaker resilience4jCircuitBreaker;

    public ClientController(
            ClientServiceCircuitBreaker clientServiceCircuitBreaker,
            Resilience4jCircuitBreaker resilience4jCircuitBreaker
    ) {
        this.clientServiceCircuitBreaker = clientServiceCircuitBreaker;
        this.resilience4jCircuitBreaker = resilience4jCircuitBreaker;
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getPersonByIdWithOwnImplOfCircuitBreaker(@PathVariable Long id) {
        return clientServiceCircuitBreaker.getPersonById(id);
    }

    @GetMapping("/resislience4j/{id}")
    @CircuitBreaker(name = "personService", fallbackMethod = "fallbackAfterRetry")
    public ResponseEntity<?> getPersonByIdWithResilience4JCircuitBreaker(@PathVariable Long id) {
        return resilience4jCircuitBreaker.getPersonById(id);
    }

    @ExceptionHandler(CallNotPermittedException.class)
    public ResponseEntity<String> handleUserNotFoundException(
            CallNotPermittedException callNotPermittedException
    ) {
        return new ResponseEntity<>(callNotPermittedException.getMessage(), HttpStatus.SERVICE_UNAVAILABLE);
    }

}
