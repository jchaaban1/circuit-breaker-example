# Sample Application for Circuit Breaker

## Overview

This Spring Boot project showcases the implementation of a Circuit Breaker pattern in a Spring Boot application through 
two illustrative examples. 
The first example features a custom Circuit Breaker implementation, while the second example leverages the 
resilience4j-spring-boot2 library to achieve a similar functionality.


## Features

- Implements a Circuit Breaker to handle latency and failures when communicating with another service.
- Provides REST endpoints to retrieve person information.
- Integrates with a simulated person service to simulate external API calls.

## Technologies Used

- Java
- Spring Boot
- Maven

## Prerequisites

- Java version 20 or higher
- Docker
- Maven

## Building and Starting the Application

To build and start the application, use the provided shell script `buildAndRun.sh`. This script performs the following steps:

1. Builds the Maven project and creates an executable JAR.
2. Creates Docker images for the application and the necessary services.
3. Starts all services and the application in Docker containers.

Run the script from the project's root directory with the following command:

```bash
./buildAndRunDockerCompose.sh
```

Then run both spring boot apps under person-service and client service

You can test the resilience4j Circuit Breaker by sending a GET request to the following URL:

http://localhost:8080/resislience4j/1

You can test the custom implementation of the Circuit Breaker by sending a GET request to the following URL:

http://localhost:8080/1

