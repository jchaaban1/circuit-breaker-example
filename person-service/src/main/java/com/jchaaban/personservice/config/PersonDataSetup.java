package com.jchaaban.personservice.config;

import com.jchaaban.personservice.entity.Person;
import com.jchaaban.personservice.repository.PersonRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonDataSetup {

    private final PersonRepository personRepository;

    public PersonDataSetup(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @PostConstruct
    public void personDataSetup() {
        for (int i = 1; i <= 10; ++i) {
            personRepository.save(new Person("firstname " + i, "Lastname " + i, 20 + i));
        }
    }
}
