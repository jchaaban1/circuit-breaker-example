package com.jchaaban.personservice.repository;

import com.jchaaban.personservice.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {
}
