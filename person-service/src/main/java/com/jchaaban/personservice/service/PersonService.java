package com.jchaaban.personservice.service;

import com.jchaaban.personservice.entity.Person;
import com.jchaaban.personservice.exception.PersonNotFoundException;
import com.jchaaban.personservice.repository.PersonRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.Random;

@Component
public class PersonService {

    private static final Logger logger = LoggerFactory.getLogger(PersonService.class);

    private final PersonRepository personRepository;
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public Person getPersonById(Long id) {
        double randomNumber = new Random().nextDouble();

        logger.info("Random number generated:" + randomNumber);

//        if (randomNumber < 0.9) {
            sleep();
//        }

        Optional<Person> optionalPerson = personRepository.findById(id);
        return optionalPerson.orElseThrow(() -> new PersonNotFoundException("Person not found with ID: " + id));
    }

    private void sleep() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }
}
